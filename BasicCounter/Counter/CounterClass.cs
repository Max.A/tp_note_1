﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Counterlib
{
    public class CounterClass
    {
        private int valeur;

        public CounterClass(int valeur)
        {
            this.valeur = valeur;
        }

        public int GetValeur()
        {
            return this.valeur;
        }

        public void SetValeur(int valeur)
        {
            this.valeur = valeur;
        }

        public int Increment()
        {
            valeur = valeur + 1;
            return valeur;
        }

        public int Decrement()
        {
            valeur = valeur - 1;
            return valeur;
        }

        public int RemiseAZero()
        {
            valeur = 0;
            return valeur;
        }
    }
}
