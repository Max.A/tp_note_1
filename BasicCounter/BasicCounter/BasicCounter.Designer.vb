﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BasicCounter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_moins = New System.Windows.Forms.Button()
        Me.btn_plus = New System.Windows.Forms.Button()
        Me.btn_raz = New System.Windows.Forms.Button()
        Me.lbl_total = New System.Windows.Forms.Label()
        Me.lbl_value = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btn_moins
        '
        Me.btn_moins.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_moins.Location = New System.Drawing.Point(60, 76)
        Me.btn_moins.Name = "btn_moins"
        Me.btn_moins.Size = New System.Drawing.Size(75, 42)
        Me.btn_moins.TabIndex = 0
        Me.btn_moins.Text = "-"
        Me.btn_moins.UseVisualStyleBackColor = True
        '
        'btn_plus
        '
        Me.btn_plus.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_plus.Location = New System.Drawing.Point(246, 76)
        Me.btn_plus.Name = "btn_plus"
        Me.btn_plus.Size = New System.Drawing.Size(75, 41)
        Me.btn_plus.TabIndex = 1
        Me.btn_plus.Text = "+"
        Me.btn_plus.UseVisualStyleBackColor = True
        '
        'btn_raz
        '
        Me.btn_raz.Location = New System.Drawing.Point(151, 143)
        Me.btn_raz.Name = "btn_raz"
        Me.btn_raz.Size = New System.Drawing.Size(75, 23)
        Me.btn_raz.TabIndex = 2
        Me.btn_raz.Text = "RAZ"
        Me.btn_raz.UseVisualStyleBackColor = True
        '
        'lbl_total
        '
        Me.lbl_total.AutoSize = True
        Me.lbl_total.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_total.Location = New System.Drawing.Point(157, 35)
        Me.lbl_total.Name = "lbl_total"
        Me.lbl_total.Size = New System.Drawing.Size(60, 25)
        Me.lbl_total.TabIndex = 3
        Me.lbl_total.Text = "Total"
        '
        'lbl_value
        '
        Me.lbl_value.AutoSize = True
        Me.lbl_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_value.Location = New System.Drawing.Point(176, 81)
        Me.lbl_value.Name = "lbl_value"
        Me.lbl_value.Size = New System.Drawing.Size(29, 31)
        Me.lbl_value.TabIndex = 4
        Me.lbl_value.Text = "0"
        '
        'BasicCounter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(371, 193)
        Me.Controls.Add(Me.lbl_value)
        Me.Controls.Add(Me.lbl_total)
        Me.Controls.Add(Me.btn_raz)
        Me.Controls.Add(Me.btn_plus)
        Me.Controls.Add(Me.btn_moins)
        Me.Name = "BasicCounter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_moins As Button
    Friend WithEvents btn_plus As Button
    Friend WithEvents btn_raz As Button
    Friend WithEvents lbl_total As Label
    Friend WithEvents lbl_value As Label
End Class
